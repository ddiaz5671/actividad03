using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;


public class Trigger01 : MonoBehaviour
{

    public PlayableDirector playable1;
    public PlayableDirector playable2;
    bool entro;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter (Collider other) {
      
        if (other.CompareTag("Player")) {

            playable1.Play();
            entro = true;
        }
    }

    void OnTriggerExit (Collider other) {
        if (!other.CompareTag("Player") && entro == true) {

            playable2.Play();
            entro = false;
        }
    }
}